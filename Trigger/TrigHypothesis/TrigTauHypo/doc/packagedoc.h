/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/**

@page TrigTauHypo_page 
@author Stefania Xella
@author M. Pilar Casado
@author Olga Igonkina

@section TrigTauHypo_MyPackageOverview Overview
This package contains HLT variables to select taus in LVL2 and EF.
Tracking and calorimeter information is used to select a narrow jet with
low multiplicity.


*/
